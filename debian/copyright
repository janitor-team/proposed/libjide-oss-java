Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Jide Common Layer (Open Source Project)
Upstream-Contact: JIDE Software, Inc.
Source: https://github.com/jidesoft/jide-oss
Comment:
 Please see README.source why the upstream version was repacked.

Files: *
Copyright: 1995,2006, Oracle and/or its affiliates
           2005-2012, Catalysoft Ltd
           2002-2017, JIDE Software, Inc.
License: GPL-2-with-classpath-exception
Comment:
 All files by Oracle and/or its affiliates are copyright
 GPL-2-with-classpath-exception despite the misleading license header which
 indicates a proprietary license. These files were taken from the OpenJDK
 project and were modified by JIDE Software, Inc. See also the official
 statement from JIDE Support at
 http://www.jidesoft.com/forum/viewtopic.php?f=18&t=14432.

Files: src/com/jidesoft/swing/CornerScroller.java
       src/com/jidesoft/swing/InfiniteProgressPanel.java
       src/com/jidesoft/plaf/aqua/AquaPreferences.java
       src/com/jidesoft/plaf/aqua/BinaryPListParser.java
Copyright:  2005, Werner Randelshofer
            2005, romain guy, craig wickesser, henry story
            2007, Davide Raccagni
License: BSD-3-clause
Comment:
 AquaPreferences.java and BinaryPListParser.java are from Quaqua (Werner
 Randelshofer) and are licensed under the BSD-3-clause. JIDE Support confirmed
 this at http://www.jidesoft.com/forum/viewtopic.php?f=18&t=14432. See also the
 License terms at http://www.randelshofer.ch/quaqua/.

Files: src/com/jidesoft/plaf/aqua/XMLElement.java
       src/com/jidesoft/plaf/aqua/XMLParseException.java
Copyright: 2000-2002, Marc De Scheemaecker
License: Zlib

Files: src/com/jidesoft/comparator/ComparableComparator.java
Copyright: 2001, The Apache Software Foundation
License: Apache-License-1.1

Files: src/com/jidesoft/comparator/AlphanumComparator.java
       src/com/jidesoft/comparator/AlphanumFileComparator.java
Copyright: 2002-2013, JIDE Software, Inc.
License: LGPL-2.1+
 On Debian systems, the complete text of the Lesser General Public License
 can be found in "/usr/share/common-licenses/LGPL-2.1".

Files: debian/*
Copyright: 2013-2021, Markus Koschany <apo@debian.org>
License: GPL-2-with-classpath-exception

License: GPL-2-with-classpath-exception
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License 2 as published by
 the Free Software Foundation.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".
 .
 "CLASSPATH" EXCEPTION TO THE GPL
 .
 Certain source files distributed by Oracle America and/or its affiliates are
 subject to the following clarification and special exception to the GPL, but
 only where Oracle has expressly included in the particular source file's header
 the words "Oracle designates this particular file as subject to the "Classpath"
 exception as provided by Oracle in the LICENSE file that accompanied this code."
 .
    Linking this library statically or dynamically with other modules is making
    a combined work based on this library.  Thus, the terms and conditions of
    the GNU General Public License cover the whole combination.
 .
    As a special exception, the copyright holders of this library give you
    permission to link this library with independent modules to produce an
    executable, regardless of the license terms of these independent modules,
    and to copy and distribute the resulting executable under terms of your
    choice, provided that you also meet, for each linked independent module,
    the terms and conditions of the license of that module.  An independent
    module is a module which is not derived from or based on this library.  If
    you modify this library, you may extend this exception to your version of
    the library, but you are not obligated to do so.  If you do not wish to do
    so, delete this exception statement from your version.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
 .
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
 .
    * Neither the name of <ORGANIZATION> nor the names of
      its contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: Zlib
 This software is provided 'as-is', without any express or implied warranty.
 In no event will the authors be held liable for any damages arising from the
 use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it freely,
 subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not claim
    that you wrote the original software. If you use this software in a product,
    an acknowledgment in the product documentation would be appreciated but is not
    required.
 .
 2. Altered source versions must be plainly marked as such, and
    must not be misrepresented as being the original software.
 .
 3. This notice may not be removed or altered from any source
    distribution.

License: Apache-License-1.1
 /* ====================================================================
 * The Apache Software License, Version 1.1
 *
 * Copyright (c) 2000 The Apache Software Foundation.  All rights
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:
 *       "This product includes software developed by the
 *        Apache Software Foundation (http://www.apache.org/)."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "Apache" and "Apache Software Foundation" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written
 *    permission, please contact apache@apache.org.
 *
 * 5. Products derived from this software may not be called "Apache",
 *    nor may "Apache" appear in their name, without prior written
 *    permission of the Apache Software Foundation.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE APACHE SOFTWARE FOUNDATION OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 * Portions of this software are based upon public domain software
 * originally written at the National Center for Supercomputing Applications,
 * University of Illinois, Urbana-Champaign.
 */
